\section*{Timing and Synchronisation}

Distributed Systems often need to be aware of time, for a number of reasons, e.g.\ recording when events occur, scheduling tasks, and handling data with time-limited validity (like caching).

Two types of clock can be distinguished --- physical clocks (counting number of seconds elapsed) and logical clocks (counting events).

In a centralised system, time is unambiguous, but can be inaccurate.
In a distributed system, there is no global agreement of time.
When each machine has its own clock, an event that occurred after another event may mistakenly be assigned an earlier time.

\subsection*{Quartz Clocks}

Quartz clocks are made from quartz crystal that is laser trimmed to resonate at a specific frequency.
An oscillator circuit produces a signal at that resonance frequency.
Counting the number of cycles allows for measurement of time.

Clock drift occurs when one might run slightly fast, and another slightly slow.
Drift is measured in parts per million (ppm) where 1ppm is equal to 1$\mu$s/s.
Most computer clocks are correct to within 50ppm.

\subsection*{Computer Clocks}

Most computers have built-in timing hardware based on a quartz crystal.
Each oscillation decrements a counter.
When this counter reaches 0, a timer interrupt fires and a clock tick occurs, resetting the counter to its initial value.
Battery-powered CMOS RAM stores the number of clock ticks since a known date, allowing the system to maintain a notion of time.
In a distributed system, all of the crystals will run at slightly different rates, resulting in clock skew.

Before 1958, the notion of time was based on the solar second.
Since the invention of the atomic clock, the value of a second is defined as the time it takes for a caesium-133 atom to make 9,192,631,770 state transitions.

The mean solar day is getting longer so if we used International Atomic Time (TAI) as described above then noon would slowly occur earlier.
To counter this, leap seconds occur whenever TAI and solar time reach an 800ms discrepancy.

The time system based on TAI with leap seconds added is known as \textbf{Coordinated Unviersal Time (UTC)}.
UTC synchronisation signals are broadcast from shortwave radio transmitters and satellites.

Computers may represent time in a number of ways, but usually in Unix time or ISO8601.
Unix time counts the number of seconds since the Unix epoch -- 1 Jan 1970 00:00:00 UTC\@.
ISO8601 follows the format \DTMnow.

A lot of software will simply ignore leap seconds.
A pragmatic way of dealing with them is to ``smear'' the leap second across the whole day.

Clock synchronisation algorithms are required to make sure machines in a distributed system have an agreed notion of time.
Cristian's algorithm is used when one machine has a UTC receiver and aims to synchronise other machines with it.
The Berkley algorithm is used when no machines have a UTC receiver and the intention is simply to minimise clock skew.

\subsection*{Synchronisation Model}

Each machine has an internal timer, issuing $H$ interrupts per second.
The interrupt handler adds 1 to the software clock keeping track of the number of ticks since some time in the past; the value of the clock is $C$.
At UTC time $t$, the value of the $i$-th clock in the system is $C_i(t)$.
Ideally, $C_i(t) = t + \gamma$ for all $i$ and $t$, and $dC/dt$ is 1.
In practice, $H$ varies slightly, causing drift.
A timer is considered to be working properly if $1 - \rho \leq dC/dt \leq 1 + \rho$, where $\rho$ is the maximum drift rate.

If two clocks are drifting from UTC in opposite directions, at a time $\Delta t$ after synchronisation they may be as much as $2\rho\Delta t$ apart.

\subsection*{Cristian's Algorithm}

The machine with the UTC receiver is the time server.
At intervals not exceeding $\Delta C/2\rho$ seconds, each machine sends a message to the time server, requesting the current time.
The time servers responds as fast as possible with $C_{UTC}$.
A fast client must gradually move to this time, to prevent time going backwards.
The algorithm must deal with the fact the time server's reply is not instant, with the delay variable on network traffic.

\begin{sequencediagram}
    \newinst{a}{Client}
    \newinst[4]{b}{Server}
    \mess[1]{a}{request: t1}{b}
    \node[anchor=east] at (mess from) {$t_1$};
    \node[anchor=west] (t2) at (mess to) {$t_2$};
    \node[anchor=west, text width=2.5cm] (irq) at ([xshift=0.15cm,yshift=-0.33cm] t2.east) {Interrupt \\ handling time};
    \mess[1]{b}{response: t1, t2, t3}{a}
    \node[anchor=west] (t3) at (mess from) {$t_3$};
    \node[anchor=east] (t4) at (mess to) {$t_4$};
\end{sequencediagram}

The round trip network delay can be calculated as $\delta = (t_4 - t_1) - (t_3 - t_2)$.
The estimated server time when the client receives the response can be calculated as $t_3 + \delta/2$.
And the clock skew can be estimated to be $\theta = t_3 + \delta/2 - t_4 = (t_2 - t_1 + t_3 - t_4)/2$.

Online sources say that the client time can just be set to $T + (t_4 - t_1)/2$, where $T$ is the time the server returns.

\subsection*{Berkley Algorithm}

This algorithm isn't as popular as it is only used when no machines have access to UTC\@.
One machine is dedicated as the time daemon.
It will request the current time from each of the other processes.
The machines return their clock value and the daemon calculates an average of all of their times.
The daemon then sends a message to each machine, telling it how much it must change its clock value by.

\subsection*{Election Algorithms}

Many distributed algorithms require one process to act as a coordinator.
It doesn't necessarily matter which process this is, one just needs to be responsible for it.
Election algorithms are responsible for selecting a unique coordinator (a leader election).
For example, if a process fails in the Berkley algorithm, another process needs to take over as master for synchronisation.
Both the Bully and Ring algorithms can be used for this.

\subsubsection*{Bully Algorithm}

Here, each process has a unique ID, and all processes know the IDs and addresses of the other processes.
Communication is assumed to be reliable.
The key idea is to select the process with the highest ID\@.
A process initiates an election if it has just recovered from failure or the current coordinator has failed.
Several processes can initiate an election simultaneously, and a consistent result must be reached.
There are three types of message available: `Election', `OK' and `I Won'.
With $n$ processes, $O(n^2)$ messages are required.

Any process $P$ can begin an election.
$P$ sends `Election' messages to all processes with higher IDs and waits for them to respond with `OK'.
If no `OK' messages are received, $P$ becomes the coordinator and sends `I Won' to all processes with lower IDs.
If it receives any `OK's, it drops out of the election and waits for an `I Won'.
If a process receives an `Election' message it returns `OK' and starts an election.
If a process receives an `I Won' it treats the sender as the new coordinator.

On the right is an example of the Bully algorithm if Process 7 has crashed.
Process 4 holds an election, and 5 and 6 inform it to stop.
5 and 6 now each hold an election, with 6 telling 5 to stop.
Finally, 6 informs all working nodes that it has won.

\begin{minipage}{0.5\linewidth}
    \centering
    \begin{tikzpicture}
        \foreach \x/\name in {0/6, 45/5, 90/1, 135/2, 180/4, 225/0, 270/7, 315/3} {
            \node[circle, draw, minimum size=0.5cm] (\name) at (\x:1.5) {\name};
        }
        \draw[red, line width=0.25pt] (7.north west) -- (7.south east);
        \draw[red, line width=0.25pt] (7.north east) -- (7.south west);

        \draw[->, >=latex] (4) -- node[midway, sloped, above, font=\small] {Election} (7);
        \draw[->, >=latex] (4) -- node[midway, sloped, above, font=\small] {Election} (6);
        \draw[->, >=latex] (4) -- node[midway, sloped, above, font=\small] {Election} (5);
    \end{tikzpicture}
\end{minipage}%
\begin{minipage}{0.5\linewidth}
    \centering
    \begin{tikzpicture}[node distance=1cm]
        \foreach \x/\name in {0/6, 45/5, 90/1, 135/2, 180/4, 225/0, 270/7, 315/3} {
            \node[circle, draw, minimum size=0.5cm] (\name) at (\x:1.5) {\name};
        }
        \draw[red, line width=0.25pt] (7.north west) -- (7.south east);
        \draw[red, line width=0.25pt] (7.north east) -- (7.south west);

        \draw[->, >=latex] (5) -- node[midway, sloped, above, font=\small] {OK} (4);
        \draw[->, >=latex] (6) -- node[midway, sloped, above, font=\small] {OK} (4);
    \end{tikzpicture}
\end{minipage}

\vspace*{-0.1cm}

\begin{minipage}{\linewidth}
    \centering
    \begin{tikzpicture}[node distance=1cm]
        \foreach \x/\name in {0/6, 45/5, 90/1, 135/2, 180/4, 225/0, 270/7, 315/3} {
            \node[circle, draw, minimum size=0.5cm] (\name) at (\x:1.5) {\name};
        }
        \draw[red, line width=0.25pt] (7.north west) -- (7.south east);
        \draw[red, line width=0.25pt] (7.north east) -- (7.south west);

        \draw[->, >=latex] (5) -- node[midway, above right, font=\small] {Election} (6);
        \draw[->, >=latex] (5) -- node[midway, sloped, above, font=\small] {Election} (7);
        \draw[->, >=latex] (6) -- (7);
    \end{tikzpicture}
\end{minipage}

\begin{minipage}{0.5\linewidth}
    \centering
    \begin{tikzpicture}[node distance=1cm]
        \foreach \x/\name in {0/6, 45/5, 90/1, 135/2, 180/4, 225/0, 270/7, 315/3} {
            \node[circle, draw, minimum size=0.5cm] (\name) at (\x:1.5) {\name};
        }
        \draw[red, line width=0.25pt] (7.north west) -- (7.south east);
        \draw[red, line width=0.25pt] (7.north east) -- (7.south west);

        \draw[->, >=latex] (6) -- node[midway, above right, font=\small] {OK} (5);
    \end{tikzpicture}
\end{minipage}%
\begin{minipage}{0.5\linewidth}
    \centering
    \begin{tikzpicture}[node distance=1cm]
        \foreach \x/\name in {0/6, 45/5, 90/1, 135/2, 180/4, 225/0, 270/7, 315/3} {
            \node[circle, draw, minimum size=0.5cm] (\name) at (\x:1.5) {\name};
        }
        \draw[red, line width=0.25pt] (7.north west) -- (7.south east);
        \draw[red, line width=0.25pt] (7.north east) -- (7.south west);

        \draw[->, >=latex] (6) -- (0);
        \draw[->, >=latex] (6) -- (1);
        \draw[->, >=latex] (6) -- (2);
        \draw[->, >=latex] (6) -- (3);
        \draw[->, >=latex] (6) -- node[midway, sloped, above, font=\small] {I Won} (4);
        \draw[->, >=latex] (6) -- (5);
    \end{tikzpicture}
\end{minipage}

\subsubsection*{Ring Algorithm}

In this algorithm, the nodes are organised into a logical ring, and each node is aware of the ones that come after it.
There are two types of message available: `Election' and `Coordinator'.

As soon as a process recognises that the current coordinator has failed, it will start an election.
It builds an `Election' message containing its own ID, and passes this to its successor.
If the successor has failed, then it skips over it, repeating this process until it reaches the next live node.
The message continues to be passed around the ring, with each live node adding its own ID to the message.
This continues around the ring until the initiating node receives a message containing its own ID\@.
At this point, the message type is changed to `Coordinator', and passed once fully around the ring.
This has the effect of informing everyone who the new coordinator is (the node in the list with the highest ID), and providing a list of all members in the current ring.

\begin{minipage}{0.5\linewidth}
    \centering
    \begin{tikzpicture}
        \foreach \x/\name in {0/2, 45/1, 90/0, 135/7, 180/6, 225/5, 270/4, 315/3} {
            \node[circle, draw, minimum size=0.5cm] (\name) at (\x:1.5) {\name};
        }
        \draw[red, line width=0.25pt] (7.north west) -- (7.south east);
        \draw[red, line width=0.25pt] (7.north east) -- (7.south west);

        \draw[->, >=latex] (4) -- node[midway, sloped, below, font=\small] {[4]} (5);
        \draw[->, >=latex] (5) -- node[midway, left, font=\small] {[4,5]} (6);
        \draw[->, >=latex] (6) -- node[midway, sloped, below, font=\small] {[4,5,6]} (0);
        \draw[->, >=latex] (0) -- node[midway, sloped, above, yshift=0.25cm, font=\small] {[4,5,6,0]} (1);
        \draw[->, >=latex] (1) -- node[midway, sloped, above, font=\small] {...} (2);
        \draw[->, >=latex] (2) -- node[midway, sloped, below, font=\small] {...} (3);
        \draw[->, >=latex] (3) -- node[midway, right, xshift=-0.15cm, yshift=-0.33cm, font=\small] {[4,5,6,0,1,2,3]} (4);
    \end{tikzpicture}
\end{minipage}%
\begin{minipage}{0.45\linewidth}
    In the example on the left, the previous coordinator, 7, has crashed, and node 4 detected this.
    It sends a message to its successor, which makes its way around the ring, skipping 7, until it reaches node 4 again.
    Then, the message type is changed to `Coordinator' and passed around once more.
    Node 6 becomes the new coordinator.
\end{minipage}

\subsection*{Network Time Protocol (NTP)}

NTP enables clients across the Internet to sync to UTC\@.
It provides a reliable service that can survive losses in connectivity.
It authenticates timing data to protect against malicious or accidental interference with the service.
Finally, it allows computers to re-sync frequently enough to offset drift rates common to most computers.

NTP servers form a logical hierarchy called a synchronisation subnet.
Primary servers (stratum 1) are connected directly to a time source, like a radio clock receiving UTC\@.
Secondary (stratum 2) servers synchronise from stratum 1, stratum 3 from stratum 2, etc.
Leaf servers execute in users' workstations.
The accuracy decreases further down the strata.

\subsubsection*{Correcting Clock Skew}

If the difference from UTC is less than 125ms, the clock can be slightly sped up or slowed down (by at most 500ppm) to bring them in sync.
If the difference is above 125ms but below 1000s, the clock is stepped to match the server's estimated timestamp.
Any further than this, a panic occurs, to be resolved by a human operator.
Systems that rely on clock sync need to monitor clock skew!
