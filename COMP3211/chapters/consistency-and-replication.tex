\section*{Consistency and Replication}

An important issue in distributed systems is the replication of data.
Data is generally replicated to enhance reliability, and improve performance.
One of the major difficulties is keeping replicas consistent.
This means that if the replica is replaced in one location, this is reflected everywhere, to prevent them becoming out of sync.

To keep replicas consistent, we generally needs to ensure conflicting operations are done in the same order everywhere.
Conflicting operations include read-write conflicts (a read and write happening concurrently) and write-write conflicts (two concurrent write operations).

The issue is, that guaranteeing global ordering on conflicting operations may be costly and degrade scalability.
The solution to this is to weaken consistency requirements to hopefully avoid global synchronisation.

\subsection*{Data-centric Consistency Models}

A \textbf{consistency model} is a contract between a (distributed) data store and a process, in which the data store precisely specifies what the results of read and write operations are in the presence of concurrency.

We can refer to a degree of consistency: replicas may differ in numerical value, relative staleness and there may be differences with respect to (number and order) of performed update operations.

Deviation in these properties are referred to as forming continuous consistency ranges.
There are different ways for applications to state what inconsistencies they can tolerate.

To define inconsistencies the \textbf{Consistency Unit (Conit)} is introduced.
This specifies the data unit over which consistency is to be measured.

For example, a fleet owner is interested how much he pays on average for gasoline.
Therefore, whenever a driver tanks gasoline, he reports the amount of gasoline that has been tanked, $g$, the price paid, $p$, and the total distance since the last time he tanked, $d$.
Technically, the variables, $g$, $p$ and $d$, form a conit that is replicated on two servers.

\subsubsection*{Continuous Consistency}

The tasks of the servers is to keep the conit ``consistently'' replicated.
$(T, R)$: an operation that was carried out at replica $R$ at its logical time $T$.

\includegraphics[width=\linewidth]{assets/continuous-consistency}

Each replica has a vector clock: (known time @ A, known time @ B).
$B$ sends $A$ an operation $[\langle 5, B \rangle: g \leftarrow g + 45]$; $A$ has made this operation permanent (it cannot be rolled back).

$A$ has three pending operations; it's order deviation is 3.
$A$ has missed two operations from $B$; max difference is $70 + 412$ units $\Rightarrow$ (2, 482).

The logical clock of $A$ is 11, the last operation from $B$ that $A$ had received had timestamp 5.
This is what the vector clock records.
The order of deviation is the number of tentative operations pretending to be committed: $A(3)$, $B(1)$.
Numerical deviation is the number of operations at all other replicas that have not yet been seen, along with the sum of the corresponding missed values: $A(2, 482)$, $B(3, 686)$.

Using these notions, it becomes possible to specify specific consistency schemes.
For example, to restrict order deviation by specifying an acceptable maximal value.
Likewise, we may want two replicas to never numerically deviate by more than 1000 units.

There is a trade-off between the cost of keeping replicas informed about how much it is deviating from other replicas, vs the cost to keep them synchronised.

\subsubsection*{Sequential Consistency}

The result of any execution is the same as if the operations of all processes were executed in some sequential order, and the operations of each individual process appear in this sequence in the order specified by its program.

Sequential consistency operations of a process are shown along a time axis.
$W_i(x)a$ denotes that process $P_i$ writes value $a$ to data item $x$.
$R_i(x)b$ denotes that process $P_i$ reads from data item $x$ and is returned $b$.
The assumption is that each data item has an initial value of $nil$.

\includegraphics[width=\linewidth]{assets/sequential-consistency}

In the example above, $P_1$ writes $a$ to $x$ locally, which will be propagated to other processes.
$P_2$ reads this value as $nil$, but then reading this value again after some time it returns the updated value of $a$ (due to propagation delay).

\includegraphics[width=\linewidth]{assets/sequential-consistency-1}

In (a), $P_1$ writes to $x$ with $a$, and then later on $P_2$ writes $b$ to $x$.
Both reading processes see the value of $b$ first, and later value $a$.
This means the write operation of $P_2$ appears to have occurred before the write operation of $P_1$.

In (b), sequential consistency is violated, because the processes don't see the same interleaving of write operations.
In this case, $P_3$ reads $b$ first, for a final value of $a$, whereas $P_4$ has the opposite result.

Another example is these 3 concurrently executing processes below:

\includegraphics[width=\linewidth]{assets/sequential-consistency-2}

There are 720 different ways these statements could be executed, but a number of them are in an invalid order (for example ones with \texttt{print(x,z)} before \texttt{y $\leftarrow$ 1}).
This results in a total of 30 execution orders being valid starting with $x \leftarrow 1$, and the same for $y$ and $z$, resulting in 90 possible valid sequences.

The contract formed between the processes and the distributed shared data store is that the processes must accept all of these sequences as valid results, and handle them correctly.

\subsubsection*{Happens-Before Relation}

An event is something happening at one node (sending a message or a local execution step).
We can say that event $a$ happens before event $b$ (denoted $a \rightarrow b$) if and only if:

- $a$ and $b$ occurred at the same node, and $a$ occurred before $b$ in that node's local exeuction order; or \\
- $a$ is the sending of some message $m$, and event $b$ is the recipient of that message; or \\
- there exists an event $c$ such that $a \rightarrow c$ and $c \rightarrow b$ (transitivity).

Happens-before is a partial order, it's possible that neither event happens before the other.
In this case, they are concurrent (denoted $a \mid\mid b$).

\includegraphics[width=\linewidth]{assets/happens-before}

In the example above, $a \rightarrow b$, $c \rightarrow d$ and $e \rightarrow f$ due to node execution order.
$b \rightarrow c$ and $d \rightarrow f$ due to messages $m1$ and $m2$.
$a \rightarrow c$, $a \rightarrow d$, $a \rightarrow f$, $b \rightarrow d$, $b \rightarrow f$, and $c \rightarrow f$ due to transitivity.
Finally, $a \mid\mid e$, $b \mid\mid e$, $c \mid\mid e$ and $d \mid\mid e$.

\subsubsection*{Causal Consistency}

With causal consistency, if event $b$ is caused by or influenced by an earlier event $a$, causality requires that everyone else first see $a$, then $b$.

For example, in a scenario where $P_1$ writes data item $x$, and then $P_2$ reads $x$ and writes $y$, the reading of $x$ and writing of $y$ are possibly causally related because the computation of $y$ may have depended on the value it read from $x$.
If two processes spontaneously and simultaneously write two different data items, these are not causally related.

This can be defined such that writes that are potentially causally related must be seen be all processes in the same order.
Concurrent writes may be seen by different processes in different orders.

\includegraphics[width=\linewidth]{assets/causal-consistency}

In the diagrams above, (a) shows a violation of causal consistency.
This is because $P_2$ reading $x$ may be used when it writes $b$, but the other processes see the writes in different orders, which is not allowed.
In (b) however, the writes are concurrent and do not potentially rely on each other, so it is fine for the other processes to see these writes in different orders.

\subsection*{Client-centric Consistency Models}

Consider a distributed database which you have access to via your laptop.
Assume your laptop acts as a frontend to the database.
At location $A$ you access the database and perform reads and updates.
At location $B$ you continue working, but unless you're connected to the same server as you were at $A$, there may be inconsistencies.

The updates may not have been propagated across, you may be reading newer entries than you were at $A$, and your work at $B$ may cause conflicts with that at $A$.

Client-centric consistency provides guarantees for a single client concerning the consistency of accesses to a data store by that client.
No guarantees are given concerning concurrent accesses by different clients.
If $P_1$ modifies data that is shared with $P_2$ but which is stored at a different location, write-write conflicts may easily be created.

$x_i$ denotes the version of data item $x$.
$L_k$ denotes the local store $k$.

$W_1(x_2)$ is the write operation by process $P_1$ that leads to version $x_2$ of $x$.
$W_1(x_i;x_j)$ indicates $P_1$ produces version $x_j$ based on a previous version $x_i$.
$W_1(x_i\mid x_j)$ indicates we do not know that $P_1$ produces version $x_j$ based on $x_i$.

\subsubsection*{Monotonic Reads}

If a process reads the value of a data item $x$, any successive read operation to $x$ by that process will always return that same, or a more recent, value.

\includegraphics[width=\linewidth]{assets/monotonic-reads}

For example in the diagrams above, the left one is monotonic-read consistent, but the right one is not.
This is because $W_2(x_1\mid x_2)$ is known to produce a version that doesn't follow from $x_1$.
As a consequence, $P_1$'s read operation at $L2$ is known not to include the write operations when it performed $R_1(x_1)$ at $L1$.

An example of when monotonic reads can be used is accessing email whilst on the move.
Each time you connect to a different email server, that server fetches all the updates from the server you previously visited.

\subsubsection*{Monotonic Writes}

A write operation by a process on data item $x$ is completed before any successive write operation on $x$ by the same process.

\includegraphics[width=\linewidth]{assets/monotonic-writes}

In (a) $P_1$ makes an update at $L1$, followed by another update later at $L2$ which follows from a write made by $P_2$ at $L2$.
This is monotonic-write consistent, because the write performed first has been propagated to $L2$, where it is used by $P_2$, and then again by $P_1$.

In (b), this propagation has not occurred, which is shown by the fact that $x_2$ is existing concurrently to $x_1$.
$P_1$ then produces $x_3$, but it is also concurrent to $x_1$ because of a lack of propagation.

In (c), we have a similar situation, but $x_3$ follows from $x_2$.
Because $x_2$ is concurrent to $x_1$ however, this means we also have $x_1 \mid x_3$ and monotonic-write consistency is violated.

In (d), again we have $x_2$ concurrently to $x_1$, but later on we have a write in which $x_3$ follows from $x_1$.
This means that $x_1$ must have been available at $L2$, meaning that a write-write conflict between $x_1$ and $x_2$ must have occurred and been resolved in favour of $x_1$.
This means that consistency does occur.
If however $P_2$ writes at $L2$ without having first read $x_1$ again, it will immediately violate consistency.

An example of monotonic writes would be updating a program at server $S_2$, and ensuring that all components on which compiling and linking depends are also placed at $S_2$.

\subsubsection*{Read your Writes}

Here, the effect of a write operation by a process on data item $x$, will always be seen by a successive read operation on $x$ by the same process.

In other words, a write operation is always completed before a read operation by the same process, no matter where the read operation takes place.

\includegraphics[width=\linewidth]{assets/monotonic-writes}

In (a), process $P_1$ wrote $x_1$ and then performed a read on another local copy.
This write operation can be seen by the succeeding read operation, which is shown by $W_2(x_1;x_2)$, meaning the write by $P_1$ has propagated to $L2$, so the following read by $P_1$ will reflect the value written by itself.

In (b), $P_2$ produces a concurrent version of $x$, meaning the previous write operation hasn't propagated to $L2$ by the time $x_2$ is produced.
This means when $P_1$ reads $x_2$, it won't reflect its previous updates, violating the consistency.

\subsection*{Replica Management}

To best place replicas, we have an optimisation problem of how to select the best $K$ locations out of $N$ possible places.
Selecting the best locations by minimal average distance to clients, and iteratively choosing the next best server is known to be computationally expensive.
Another method is to select the $K$-th largest autonomous system and place a server at the best connected host.
This is also computationally expensive, however.
Alternatively we can place nodes in an $m$-dimensional geometric space, where distance between nodes reflects latency.
We can then identify the $K$ regions with the highest density and place replicas here.
This is computationally cheap.

\subsubsection*{Content Replication}

\includegraphics[width=\linewidth]{assets/content-replication}

Permanent replicas are processes or machines always having a replica.
Server-initiated replicas are processes that can dynamically host a replica on request of another server in the data store.
Client-initiated replicas are processes that can dynamically host a replica on request of a client (client cache).

Server-initiated replicas can be managed in the following manner:
We keep track of access counts per file, grouped by considering the server closest to requesting clients.
If the number of accesses drops below the deletion threshold $D$, the replica drops the file.
If it exceeds the replication threshold $R$, the file is replicated.
If the number of accesses is between $D$ and $R$, the file is migrated.

\includegraphics[width=\linewidth]{assets/server-initiated-replica}

\subsection*{Consistency Protocols}

One option for this is \textbf{primary-backup protocols}.
Here writes to a data item $x$ must be performed at the primary for that item $x$.
Reads can happen locally.
A process wanting to perform the write operation will forward it to the primary, which will update its copy and tell the backups to perform this update.
Once the backups are all up to date, they inform the primary, which informs the client that the process is complete.

\includegraphics[width=\linewidth]{assets/primary-backup-protocols}

This is traditionally applied in distributed databases, and file systems that require a high-degree of fault tolerance.
Replicas are often placed on the same LAN\@.

Another option is \textbf{primary-backup protocols with local writes}.
The difference here is that when a write needs to happen, the primary for $x$ is located, and moved to the writing process' own location.
The advantage here is that writes can now happen locally for this process, whilst other reads can still access their local copy as before.

\includegraphics[width=\linewidth]{assets/primary-backup-local-writes}

An example of where this can be used is mobile computers in disconnected mode.
Before they disconnect they become the primary for files they need to use, and other machines can still read their local copy (but not perform updates).
When the mobile machine reconnects, its updates can be propagated to the backups.
