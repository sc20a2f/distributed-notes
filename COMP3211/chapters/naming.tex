\section*{Naming}

\textbf{Entities} must be given \textbf{names} to be able to access them.
Entities are resources like computers, printers, webpages, etc.
A name is a string of bits or characters (optionally human-readable).

Names must be bound to the attributes of an entity, normally one of those attributes will give access to the entity.
A name is \textbf{resolved} when it is translated to those attributes.
In a distributed system, the implementation of a naming system may be described across multiple machines.

To operate on an entity, another special type of entity called an \textbf{access point} is required.
The name of an access point is called the \textbf{address}, which is usually thought of as the address of the entity itself.
An entity can have more than one access point, and they may change over time.
It is good practice not to use the address as a regular name for an entity, instead it is better to use one that is location independent.

A \textbf{naming system} manages the set of bindings between names and attributes of entities in a system.
Its primary function is name resolution, but they may also support creation and deletion of bindings, organisation of the namespace and listing bound names.

\subsection*{Flat Naming Systems}

Entities are referred to by an identifier which in principle has no meaning.
These systems don't have a structure and need special mechanisms to locate entities.

\subsubsection*{Broadcasting}

Here, the ID of an entity is broadcast to the network, requesting for it to return its current address.
This is only possible with local area networks, and requires all processes to be listening for these incoming requests.

An example is ARP, which sends ``Who has IP address x.x.x.x?'' requests to find the corresponding MAC address.

\subsubsection*{Forwarding Pointers}

When an entity moves from A, it leaves behind a pointer to its next location B\@.
Dereferencing can be made transparent to users by just following the pointer chain.
When an entity is found the client can update its reference.
This system has geographical scaling problems -- long chains are not fault-tolerant and dereferencing increases network latency.

An example is Stub-Scion Pair (SSP) Chains.
These use a client and server stub, which allow the storing of entity references.

\vspace*{-0.4cm}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.8\linewidth]{assets/ssp}
    \label{fig:ssp}
\end{figure}

\subsubsection*{Home-based Approach}

Here, a home keeps track of where an entity is, with each mobile host using a fixed IP address.
Communication to the IP is initially directed to the host's home agent.
This home agent is located on the local area network which corresponds to the network address for the mobile host.
Whenever the mobile host moves to another network, it requests a (temporary) foreign address.
This is referred to as a `care-of address' and is stored by the home agent.

\includegraphics[width=\linewidth]{assets/home-based}

The mechanisms to deliver packets to the mobile host is largely hidden from applications -- it has a high degree of transparency.

\subsubsection*{Distributed Hash Tables}

Consider the organisation of many nodes into a logical ring.
Each node is given a random $m$-bit identifier, and every entity is given a unique $m$-bit key.
An entity with key $k$ falls under the jurisdiction of node with smallest ID $k$, called its successor, $succ(k)$.

Each node $p$ maintains a finger table, $FT_p[]$, with at most $m$ entries, such that $FT_p[i] = succ(p + 2^{i-1})$.

The i-th entry points to the first node succeeding $p$ by at least $2^{i-1}$.

To look up key $K$, node $p$ forwards the request to the node with index $j$, satisfying $q = FT_p[j] \leq k < FT_p[j+1]$.
If $p < k < FT_p[1]$, the request is also forwarded to $FT_p[1]$.

\includegraphics[width=\linewidth]{assets/dht}

\subsubsection*{Hierarchical Approaches}

The idea here is to build a large-scale search tree for which the underlying network is split into hierarchical domains.
Each domain is represented by a different directory node, allowing for a tree to be formed.

The address of entity $E$ is stored in a leaf or intermediate node.
Intermediate nodes contain a pointer to a child if and only if the subtree rooted at the child stores an address of the entity.
The root node knows about all entities.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.8\linewidth]{assets/tree-example}
    \caption*{Example: Storing information of entity $E$ having 2 addresses in different leaf domains ($E$ is replicated in D1 and D2)}
    \label{fig:tree-example}
\end{figure}

\vspace*{-0.25cm}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.8\linewidth]{assets/tree-lookup}
    \caption*{Lookup Operation}
    \label{fig:tree-lookup}
\end{figure}

\vspace*{-0.25cm}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/tree-insert}
    \caption*{Insert Operation}
    \label{fig:tree-insert}
\end{figure}

Flat names are good for machines, but generally not very convenient for humans.
Naming systems support structured naming, which are composed of human-readable names.

A \textbf{namespace} is the collection of all valid names recognised by a particular service.
Namespaces for structured names can be shown as a directed graph, with leaf nodes and directory nodes.
Leaf nodes store entity attributes or the entity state itself.
Directory nodes store a directory table of outgoing edges.

\includegraphics[width=\linewidth]{assets/naming-graph}

\textbf{Name resolution} is the process of looking up a name, the \textbf{closure mechanism} is the mechanism to select the implicit context from which to start resolution.
Name resolution is only possible if we know how and where to start.

For example, in the Unix file system the inode of the root directory is the first inode of the logical disk, and its byte offset can be calculated from information in the superblock.
Every file and directory is represented by an inode.
A directory is a flat file of fixed-size entries -- containing a file name and corresponding inode.

Aliases can be created for named entities, which correspond to links in the graph.
Multiple absolute path names can refer to the same node (hard linking).
Or, the leaf node of the alias can store the absolute pathname to the aliased entity (symbolic linking).

Name resolution can be used to merge two different namespaces in a transparent way via mounting.
Here, a node identifier of one namespace is associated with the node identifier in the current namespace.

The \textbf{foreign namespace} is the one which needs to be accessed, the \textbf{mount point} is the node in the current namespace containing the node identifier of the foreign namespace, and the \textbf{mounting point} is the node in the foreign namespace from where to resume name resolution.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/nfs}
    \caption*{Resolving \texttt{/remote/vu/mbox} using mounting}
    \label{fig:nfs}
\end{figure}

When implementing namespaces, we can distribute name resolution and namespace management across multiple machines, by distributing nodes of the naming graph.
Three levels can be distinguished: Global -- this consists of high level directory nodes.
The main aspect is that these nodes have to be jointly managed by different administrators.
Administrational -- this contains mid-level directory nodes, that can be grouped in a manner that allows each group to be assigned a separate administration.
Managerial -- this contains low-level directory nodes, within a single administration.
The main issue here is effectively mapping directory nodes to local nameservers.

\begin{tabular}{|l|l|l|l|}
    \hline
    \textbf{Item} & \textbf{Global} & \textbf{Administrational} & \textbf{Managerial} \\
    \hline
    Scale & World & Organisation & Department \\
    \hline
    Nodes & Few & Many & Loads \\
    \hline
    Lookup & Seconds & Milliseconds & Instant \\
    \hline
    Update & Lazy & Instant & Instant \\
    \hline
    Replicas & Many & None/Few & None \\
    \hline
    Caching & Yes & Yes & Sometimes \\
    \hline
\end{tabular}

\subsubsection*{DNS Zone Data}

With DNS, zones contain attribute data for names in a domain.
This includes the names and IP addresses for at least \textbf{two} nameservers with authoritative data for the zone.
The names and IP addresses of authoritative nameservers for delegated subdomains.
Parameters governing the caching and replication of zone data.

Any nameserver is free to cache data from others, as long as the client is informed it's from a non-authoritative source.
Each entry in a zone has a Time-to-live (TTL), cached data can be provided to clients for up to this amount of time, after which it must be re-fetched from the authoritative NS.

\subsubsection*{Name Resolution Methods}

Name resolution can be performed iteratively or recursively.
In iterative resolution, the root server will resolve the name as far as it can, and then return the result to the client.
The client then continues resolution by passing the remaining path to the next NS\@.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/iterative}
    \caption*{Iterative Resolution}
    \label{fig:iterative}
\end{figure}

With recursive resolution, the nameservers pass the result to the next nameserver it finds, until resolution is complete.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/recursive}
    \caption*{Recursive Resolution}
    \label{fig:recursive}
\end{figure}

Recursive resolution places more load on each nameserver, so servers which require high throughput might only allow for iterative.
Caching in recursive is much more efficient, because each NS will gradually learn the addresses of nameservers handling lower-level nodes.
With iterative resolution, caching is limited to the client's resolver, so other clients can't benefit from it.
A compromise is to have a local naming server shared by all clients.

\subsection*{Directory Services}

DNS is a traditional naming service, which given a name resolves it to a node in the graph.
Conceptually it is the same as a telephone directory -- names to numbers.

A directory service is a different approach, implemented as a (distributed) database.
A description is stored at each node and a partial description can be used to retrieve nodes.
Entities are looked up by means of their attributes, making it conceptually similar to the Yellow Pages.

\textbf{LDAP} (Lightweight Directory Access Protocol), implemented directly on top of TCP, provides a simple protocol for accessing services over the Internet.
It's seen as a standard for Internet-based directory services.

Each directory entry contains attribute-value pairs, which are uniquely named to assist in lookups.
The \textbf{Directory Information Base} is the collection of all entries in an LDAP service.
Each record is uniquely named as a sequence of attributes (called the \textbf{Relative Distinguished Name}), so that it can be looked up.
The naming graph of an LDAP service is called the \textbf{Directory Information Tree}, each node representing an entry.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/ldap}
    \caption*{Part of a Directory Information Tree}
    \label{fig:ldap}
\end{figure}

Java provides the Java Naming and Directory Interface (JNDI) as an API for directory services that allows clients to discover and lookup objects via a name.
JNDI is independent of the underlying implementation, and provides a Service Provider Interface, allowing directory service implementations to be supported by the framework.
The API provides functionality to bind objects to names, a lookup interface, and an event handling interface to observe modifications.
The LDAP service provider for the JNDI comes as standard in the Java SDK.
